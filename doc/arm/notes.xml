<!DOCTYPE book [
<!ENTITY Scaron "&#x160;">
<!ENTITY scaron "&#x161;">
<!ENTITY ccaron "&#x10D;">
<!ENTITY aacute "&#x0E1;">
<!ENTITY iacute "&#x0ED;">
<!ENTITY mdash "&#8212;">
<!ENTITY ouml "&#xf6;">]>
<!--
 - Copyright (C) 2014-2018  Internet Systems Consortium, Inc. ("ISC")
 -
 - This Source Code Form is subject to the terms of the Mozilla Public
 - License, v. 2.0. If a copy of the MPL was not distributed with this
 - file, You can obtain one at http://mozilla.org/MPL/2.0/.
-->

<section xmlns:db="http://docbook.org/ns/docbook" version="5.0"><info/>
  <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="noteversion.xml"/>
  <section xml:id="relnotes_intro"><info><title>Introduction</title></info>
    <para>
      BIND 9.12.0 is a new feature release of BIND.  This document
      summarizes new features and functional changes that have been
      introduced on this branch, as well as features that have been
      deprecated or removed.
    </para>
  </section>

  <section xml:id="relnotes_download"><info><title>Download</title></info>
    <para>
      The latest versions of BIND 9 software can always be found at
      <link xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="http://www.isc.org/downloads/">http://www.isc.org/downloads/</link>.
      There you will find additional information about each release,
      source code, and pre-compiled versions for Microsoft Windows
      operating systems.
    </para>
  </section>

  <section xml:id="relnotes_features"><info><title>New Features</title></info>
    <itemizedlist>
      <listitem>
	<para>
	  Many aspects of <command>named</command> have been modified
	  to improve query performance, and in particular, performance
	  for delegation-heavy zones:
	</para>
	<itemizedlist>
	  <listitem>
	    <para>
	      The additional cache ("acache") was found not to
	      significantly improve performance and has been removed.
	      As a result, the <command>acache-enable</command> and
	      <command>acache-cleaning-interval</command> options no longer
	      have any effect. For backwards compatibility, BIND will
	      accept their presence in a configuration file, but
	      will log a warning.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      In place of the acache, <command>named</command> can now use
	      a glue cache to speed up retrieval of glue records when sending
	      delegation responses.  Unlike acache, this feature is on by
	      default; use <command>glue-cache no;</command> to disable it.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <command>minimal-responses</command> is now set
	      to <literal>no-auth-recursive</literal> by default.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      The <command>additional-from-cache</command>
	      and <command>additional-from-auth</command> options no longer
	      have any effect. <command>named</command> will log a warning
	      if they are set.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      Several functions have been refactored to improve
	      performance, including name compression, owner name
	      case restoration, hashing, and buffers.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      When built with default <command>configure</command> options,
	      <command>named</command> no longer fills memory with tag
	      values when allocating or freeing it. This improves performance,
	      but makes it more difficult to debug certain memory-related
	      errors. The default is reversed if building with developer
	      options. <command>named -M fill</command> or
	      <command>named -M nofill</command> will set the behavior
	      accordingly regardless of build options.
	    </para>
	  </listitem>
	</itemizedlist>
      </listitem>
      <listitem>
	<para>
	  Several areas of code have been refactored for improved
	  readability, maintainability, and testability:
	</para>
	<itemizedlist>
	  <listitem>
	    <para>
	      The <command>named</command> query logic implemented in
	      <command>query_find()</command> has been split into
	      smaller functions with a context structure to maintain state
	      between them, and extensive comments have been added.
	      [RT #43929]
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      Similarly the iterative query logic implemented in
	      <command>resquery_response()</command> function has been
	      split into smaller functions and comments added. [RT #45362]
	    </para>
	  </listitem>
	</itemizedlist>
      </listitem>
      <listitem>
	<para>
	  Code implementing name server query processing has been moved
	  from <command>named</command> to an external library,
	  <command>libns</command>. This will make it easier to
	  write unit tests for the code, or to link it into new tools.
	  [RT #45186]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>named</command> can now synthesize negative responses
	  (NXDOMAIN, NODATA, or wildcard answers) from cached DNSSEC-verified
	  records that were returned in negative or wildcard responses from
	  authoritative servers.
	</para>
	<para>
	  This will reduce query loads on authoritative servers for signed
	  domains: when existing cached records can be used by the resolver
	  to determine that a name does not exist in the authoritative domain,
	  no query needs to be sent. Reducing the number of iterative queries
	  should also improve resolver performance.
	</para>
	<para>
	  This behavior is controlled by the new
	  <filename>named.conf</filename> option
	  <command>synth-from-dnssec</command>.  It is enabled by
	  default.
	</para>
	<para>
	  Note: this currently only works for zones signed using NSEC.
	  Support for zones signed using NSEC3 (without opt-out) is
	  planned for the future.
	</para>
	<para>
	  Thanks to APNIC for sponsoring this work.
	</para>
      </listitem>
      <listitem>
	<para>
	  When acting as a recursive resolver, <command>named</command>
	  can now continue returning answers whose TTLs have expired
	  when the authoritative server is under attack and unable to
	  respond. This is controlled by the
	  <command>stale-answer-enable</command>,
	  <command>stale-answer-ttl</command> and
	  <command>max-stale-ttl</command> options. [RT #44790]
	</para>
      </listitem>
      <listitem>
	<para>
	  The DNS Response Policy Service (DNSRPS) API, a mechanism to
	  allow <command>named</command> to use an external response policy
	  provider, is now supported. (One example of such a provider is
	  "FastRPZ" from Farsight Security, Inc.) This allows the same
	  types of policy filtering as standard RPZ, but can reduce the
	  workload for <command>named</command>, particularly when using
	  large and frequently-updated policy zones. It also enables
	  <command>named</command> to share response policy providers
	  with other DNS implementations such as Unbound.
	</para>
	<para>
	  This feature is available if BIND is built with
	  <command>configure --enable-dnsrps</command>, if a DNSRPS
	  provider is installed, and if <command>dnsrps-enable</command>
	  is set to "yes" in <filename>named.conf</filename>. Standard
	  built-in RPZ is used otherwise.
	</para>
	<para>
	  Thanks to Farsight Security for the contribution. [RT #43376]
	</para>
      </listitem>
      <listitem>
	<para>
	  Setting <command>max-journal-size</command> to
	  <literal>default</literal> limits journal sizes to twice the
	  size of the zone contents.  This can be overridden by setting
	  <command>max-journal-size</command> to <literal>unlimited</literal>
	  or to an explicit value up to 2G. Thanks to Tony Finch for
	  the contribution. [RT #38324]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>dnstap</command> logfiles can now be configured to
	  automatically roll when they reach a specified size. If
	  <command>dnstap-output</command> is configured with mode
	  <literal>file</literal>, then it can take optional
	  <command>size</command> and <command>versions</command>
	  key-value arguments to set the logfile rolling parameters.
	  (These have the same semantics as the corresponding
	  options in a <command>logging</command> channel statement.)
	  [RT #44502]
	</para>
      </listitem>
      <listitem>
	<para>
	  Logging channels and <command>dnstap-output</command> files can
	  now be configured with a <command>suffix</command> option,
	  set to either <literal>increment</literal> or
	  <literal>timestamp</literal>, indicating whether log files
	  should be given incrementing suffixes when they roll
	  over (e.g., <filename>logfile.0</filename>,
	  <filename>.1</filename>, <filename>.2</filename>, etc)
	  or suffixes indicating the time of the roll. The default
	  is <literal>increment</literal>.  [RT #42838]
	</para>
      </listitem>
      <listitem>
	<para>
	  The <command>print-time</command> option in the
	  <command>logging</command> configuration can now take arguments
	  <userinput>local</userinput>, <userinput>iso8601</userinput> or
	  <userinput>iso8601-utc</userinput> to indicate the format in
	  which the date and time should be logged. For backward
	  compatibility, <userinput>yes</userinput> is a synonym for
	  <userinput>local</userinput>.  [RT #42585]
	</para>
      </listitem>
      <listitem>
	<para>
	  The new <command>dnssec-cds</command> command generates a new DS
	  set to place in a parent zone, based on the contents of a child
	  zone's validated CDS or CDNSKEY records. It can produce a
	  <filename>dsset</filename> file suitable for input to
	  <command>dnssec-signzone</command>, or a series of
	  <command>nsupdate</command> commands to update the parent zone
	  via dynamic DNS. Thanks to Tony Finch for the contribution.
	  [RT #46090]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>nsupdate</command> and <command>rndc</command> now accept
	  command line options <command>-4</command> and <command>-6</command>
	  which force using only IPv4 or only IPv6, respectively. [RT #45632]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>nsec3hash -r</command> ("rdata order") takes arguments
	  in the same order as they appear in NSEC3 or NSEC3PARAM records.
	  This makes it easier to generate an NSEC3 hash using values cut
	  and pasted from an existing record. Thanks to Tony Finch for
	  the contribution. [RT #45183]
	</para>
      </listitem>
      <listitem>
	<para>
	  The <command>new-zones-directory</command> option allows
	  <command>named</command> to store configuration parameters
	  for zones added via <command>rndc addzone</command> in a
	  location other than the working directory. Thanks to Petr
	  Men&scaron;&iacute;k of Red Hat for the contribution.
	  [RT #44853]
	</para>
      </listitem>
      <listitem>
	<para>
	  The <command>dnstap-read -x</command> option prints a hex
	  dump of the wire format DNS message encapsulated in each
	  <command>dnstap</command> log entry. [RT #44816]
	</para>
      </listitem>
      <listitem>
	<para>
	  The <command>host -A</command> option returns most
	  records for a name, but omits types RRSIG, NSEC and NSEC3.
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>dig +ednsopt</command> now accepts the names
	  for EDNS options in addition to numeric values. For example,
	  an EDNS Client-Subnet option could be sent using
	  <command>dig +ednsopt=ecs:...</command>. Thanks to
	  John Worley of Secure64 for the contribution. [RT #44461]
	</para>
      </listitem>
      <listitem>
	<para>
	  Added support for the EDNS TCP Keepalive option (RFC 7828);
	  this allows negotiation of longer-lived TCP sessions
	  to reduce the overhead of setting up TCP for individual
	  queries. [RT #42126]
	</para>
      </listitem>
      <listitem>
	<para>
	  Added support for the EDNS Padding option (RFC 7830),
	  which obfuscates packet size analysis when DNS queries
	  are sent over an encrypted channel. [RT #42094]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>rndc</command> commands which refer to zone names
	  can now reference a zone of type <command>redirect</command>
	  by using the special zone name "-redirect". (Previously this
	  was not possible because <command>redirect</command> zones
	  always have the name ".", which can be ambiguous.)
	</para>
	<para>
	  In the event you need to manipulate a zone actually
	  called "-redirect", use a trailing dot: "-redirect."
	</para>
	<para>
	  Note: This change does not apply to the
	  <command>rndc addzone</command> or
	  <command>rndc modzone</command> commands.
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>named-checkconf -l</command> lists the zones found
	  in <filename>named.conf</filename>. [RT #43154]
	</para>
      </listitem>
      <listitem>
	<para>
	  Query logging now includes the ECS option, if one was
	  present in the query, in the format
	  "[ECS <replaceable>address/source/scope</replaceable>]".
	</para>
      </listitem>
      <listitem>
	<para>
	  By default, BIND now uses the random number generation functions
	  in the cryptographic library (i.e., OpenSSL or a PKCS#11
	  provider) as a source of high-quality randomness rather than
	  <filename>/dev/random</filename>.  This is suitable for virtual
	  machine environments, which may have limited entropy pools and
	  lack hardware random number generators.
	</para>
	<para>
	  This can be overridden by specifying another entropy source via
	  the <command>random-device</command> option in
	  <filename>named.conf</filename>, or via the <command>-r</command>
	  command line option.  However, for functions requiring full
	  cryptographic strength, such as DNSSEC key generation, this
	  <emphasis>cannot</emphasis> be overridden. In particular, the
	  <command>-r</command> command line option no longer has any
	  effect on <command>dnssec-keygen</command>.
	</para>
	<para>
	  This can be disabled by building with
	  <command>configure --disable-crypto-rand</command>, in which
	  case <filename>/dev/random</filename> will be the default
	  entropy source.  [RT #31459] [RT #46047]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>rndc managed-keys destroy</command> shuts down all
	  RFC 5011 DNSSEC trust anchor maintenance, and deletes any
	  existing managed keys database. If immediately followed by
	  <command>rndc reconfig</command>, this will reinitialize
	  key maintenance just as if the server was being started for
	  the first time.
	</para>
	<para>
	  This is intended for testing purposes, but can be used -- with
	  extreme caution -- as a brute-force repair for unrecoverable
	  problems with a managed keys database, to jumpstart the key
	  acquisition process if <filename>bind.keys</filename> is updated,
	  etc. [RT #32456]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>dnssec-signzone -S</command> can now add or remove
	  synchronization records (CDS and CDNSKEY) based on key metadata
	  set by the <command>-Psync</command> and <command>-Dsync</command>
	  options to <command>dnssec-keygen</command>,
	  <command>dnssec-settime</command>, etc. [RT #46149]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>dnssec-checkds -s</command> specifies a file from
	  which to read a DS set rather than querying the parent zone.
	  This can be used to check zone correctness prior to
	  publication. Thanks to Niall O'Reilly [RT #44667]
	</para>
      </listitem>
    </itemizedlist>
  </section>

  <section xml:id="relnotes_removed"><info><title>Removed Features</title></info>
    <itemizedlist>
      <listitem>
	<para>
	  As noted above, the <command>acache-enable</command>,
	  <command>acache-cleaning-interval</command>,
	  <command>additional-from-cache</command> and
	  <command>additional-from-auth</command> options are no longer
	  effective and <command>named</command> will log a warning if
	  they are set.
	</para>
      </listitem>
      <listitem>
	<para>
	  The ISC DNSSEC Lookaside Validation (DLV) service has
	  been shut down; all DLV records in the dlv.isc.org zone
	  have been removed.  References to the service have been
	  removed from BIND documentation.  Lookaside validation
	  is no longer used by default by <command>delv</command>.
	  The DLV key has been removed from <filename>bind.keys</filename>.
	  Setting <command>dnssec-lookaside</command> to
	  <command>auto</command> or to use dlv.isc.org as a trust
	  anchor results in a warning being issued.
	</para>
      </listitem>
      <listitem>
	<para>
	  The lightweight resolver daemon and library (<command>lwresd</command>
	  and <command>liblwres</command>) have been removed. [RT #45186]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>dig +sigchase</command> and related options
	  <command>+trusted-keys</command> and <command>+topdown</command>
	  have been removed. <command>delv</command> is now the recommended
	  command for looking up records with DNSSEC validation.
	  [RT #42793]
	</para>
      </listitem>
      <listitem>
	<para>
	  The use of <command>dnssec-keygen</command> to generate
	  HMAC keys for TSIG authentication has been deprecated in favor
	  of <command>tsig-keygen</command>. If the algorithms HMAC-MD5,
	  HMAC-SHA1, HMAC-SHA224, HMAC-SHA256, HMAC-SHA384, or
	  HMAC-SHA512 are specified, <command>dnssec-keygen</command>
	  will print a warning message. These algorithms will be
	  removed from <command>dnssec-keygen</command> entirely in
	  a future release. [RT #42272]
	</para>
      </listitem>
      <listitem>
	<para>
	  The use of HMAC-MD5 for RNDC keys is no longer recommended.
	  The default algorithm generated by <command>rndc-confgen</command>
	  is now HMAC-SHA256. [RT #42272]
	</para>
      </listitem>
      <listitem>
	<para>
	  The <command>isc-hmac-fixup</command> command, which was created
	  to address an interoperability problem in TSIG keys between
	  early versions of BIND and other DNS implementations, is now
	  obsolete and has been removed. [RT #46411]
	</para>
      </listitem>
      <listitem>
	<para>
	  Windows XP and Windows 2003 are no longer supported platforms for
	  BIND; "XP" binaries are no longer available for download from
	  ISC.
	</para>
      </listitem>
    </itemizedlist>
  </section>

  <section xml:id="proto_changes"><info><title>Protocol Changes</title></info>
    <itemizedlist>
      <listitem>
	<para>
	  BIND can now use the Ed25519 and Ed448 Edwards Curve DNSSEC
	  signing algorithms described in RFC 8080. Note, however, that
	  these algorithms must be supported in OpenSSL;
	  currently they are only available in the development branch
	  of OpenSSL at
	  <link xmlns:xlink="http://www.w3.org/1999/xlink"
	    xlink:href="https://github.com/openssl/openssl">
	    https://github.com/openssl/openssl</link>.
	  [RT #44696]
	</para>
      </listitem>
      <listitem>
	<para>
	  When parsing DNS messages, EDNS KEY TAG options are checked
	  for correctness. When printing messages (for example, in
	  <command>dig</command>), EDNS KEY TAG options are printed
	  in readable format.
	</para>
      </listitem>
    </itemizedlist>
  </section>

  <section xml:id="relnotes_changes"><info><title>Feature Changes</title></info>
    <itemizedlist>
      <listitem>
	<para>
	  <command>named</command> will no longer start or accept
	  reconfiguration if the working directory (specified by the
	  <command>directory</command> option) or the managed-keys
	  directory (specified by <command>managed-keys-directory</command>
	  are not writable by the effective user ID. [RT #46077]
	</para>
      </listitem>
      <listitem>
	<para>
	  Initializing keys specified in a <command>managed-keys</command>
	  statement or by <command>dnssec-validation auto;</command> are
	  now tagged as "initializing", until they have been updated by a
	  key refresh query. If key maintenance fails to initialize,
	  this will be visible when running <command>rndc secroots</command>.
	  [RT #46267]
	</para>
      </listitem>
      <listitem>
	<para>
	  Previously, <command>update-policy local;</command> accepted
	  updates from any source so long as they were signed by the
	  locally-generated session key. This has been further restricted;
	  updates are now only accepted from locally configured addresses.
	  [RT #45492]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>dnssec-keygen</command> no longer has default
	  algorithm settings. It is necessary to explicitly specify the
	  algorithm on the command line with the <command>-a</command> option
	  when generating keys. This may cause errors with existing signing
	  scripts if they rely on current defaults. The intent is to
	  reduce the long-term cost of transitioning to newer algorithms in
	  the event of RSASHA1 being deprecated. [RT #44755]
	</para>
      </listitem>
      <listitem>
	<para>
	  The Response Policy Zone (RPZ) implementation has been
	  substantially refactored: updates to the RPZ summary
	  database are no longer directly performed by the zone
	  database but by a separate function that is called when
	  a policy zone is updated.  This improves both performance
	  and reliability when policy zones receive frequent updates.
	  Summary database updates can be rate-limited by using the
	  <command>min-update-interval</command> option in a
	  <command>response-policy</command> statement. [RT #43449]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>dnstap</command> now stores both the local and remote
	  addresses for all messages, instead of only the remote address.
	  The default output format for <command>dnstap-read</command> has
	  been updated to include these addresses, with the initiating
	  address first and the responding address second, separated by
	  "-&gt;" or "&lt;-" to indicate in which direction the message
	  was sent. [RT #43595]
	</para>
      </listitem>
      <listitem>
	<para>
	  Expanded and improved the YAML output from
	  <command>dnstap-read -y</command>: it now includes packet
	  size and a detailed breakdown of message contents.
	  [RT #43622] [RT #43642]
	</para>
      </listitem>
      <listitem>
	<para>
	  Threads in <command>named</command> are now set to human-readable
	  names to assist debugging on operating systems that support that.
	  Threads will have names such as "isc-timer", "isc-sockmgr",
	  "isc-worker0001", and so on. This will affect the reporting of
	  subsidiary thread names in <command>ps</command> and
	  <command>top</command>, but not the main thread. [RT #43234]
	</para>
      </listitem>
      <listitem>
	<para>
	  If an ACL is specified with an address prefix in which the
	  prefix length is longer than the address portion (for example,
	  192.0.2.1/8), it will now be treated as a fatal error during
	  configuration. [RT #43367]
	</para>
      </listitem>
      <listitem>
	<para>
	  <command>dig</command> now warns about .local queries which are
	  reserved for Multicast DNS. [RT #44783]
	</para>
      </listitem>
      <listitem>
	<para>
	  The view associated with the query is now logged unless it
	  it is "_default/IN" or "_dnsclient/IN" when logging DNSSEC
	  validator messages.
	</para>
      </listitem>
      <listitem>
	<para>
	  When <command>named</command> was reconfigured, failure of some
	  zones to load correctly could leave the system in an inconsistent
	  state; while generally harmless, this could lead to a crash later
	  when using <command>rndc addzone</command>.  Reconfiguration changes
	  are now fully rolled back in the event of failure. [RT #45841]
	</para>
      </listitem>
      <listitem>
	<para>
	  Multiple <command>cookie-secret</command> clauses are now
	  supported.  The first <command>cookie-secret</command> in
	  <filename>named.conf</filename> is used to generate new
	  server cookies.  Any others are used to accept old server
	  cookies or those generated by other servers using the
	  matching <command>cookie-secret</command>.
	</para>
      </listitem>
      <listitem>
	<para>
	  A new statistics counter has been added to track prefetch
	  queries. [RT #45847]
	</para>
      </listitem>
      <listitem>
	<para>
	  A new statistics counter has been added to track priming
	  queries. [RT #46313]
	</para>
      </listitem>
      <listitem>
	<para>
	  The <command>dnssec-signzone -x</command> flag and the
	  <command>dnssec-dnskey-kskonly</command> option in
	  <command>named.conf</command>, which suppress the use of
	  the ZSK when signing DNSKEY records, now also apply to
	  CDNSKEY and CDS records. Thanks to Tony Finch for the
	  contribution. [RT #45689]
	</para>
      </listitem>
      <listitem>
	<para>
	  Trust anchor telemetry messages, as specified by
	  RFC 8145, are now logged to the
	  <command>trust-anchor-telemetry</command> logging
	  category.
	</para>
      </listitem>
      <listitem>
	<para>
	  The <command>filter-aaaa-on-v4</command> and
	  <command>filter-aaaa-on-v6</command> options are no longer
	  conditionally compiled in <command>named</command>. [RT #46340]
	</para>
      </listitem>
    </itemizedlist>
  </section>

  <section xml:id="relnotes_license"><info><title>License</title></info>
    <para>
      BIND is open source software licenced under the terms of the Mozilla
      Public License, version 2.0 (see the <filename>LICENSE</filename>
      file for the full text).
    </para>
    <para>
      The license requires that if you make changes to BIND and distribute
      them outside your organization, those changes must be published under
      the same license. It does not require that you publish or disclose
      anything other than the changes you have made to our software.  This
      requirement does not affect anyone who is using BIND, with or without
      modifications, without redistributing it, nor anyone redistributing
      BIND without changes.
    </para>
    <para>
      Those wishing to discuss license compliance may contact ISC at
      <link
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xlink:href="https://www.isc.org/mission/contact/">
	https://www.isc.org/mission/contact/</link>.
    </para>
  </section>

  <section xml:id="end_of_life"><info><title>End of Life</title></info>
    <para>
      The end-of-life date for BIND 9.12 has not yet been determined.
      However, it is not intended to be an Extended Support Version (ESV)
      branch; accordingly, support will end after the next stable
      branch (9.14) becomes available.  Those needing a longer-lived
      branch are encouraged to use the current ESV, BIND 9.11, which
      will be supported until December 2021. See
      <link xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://www.isc.org/downloads/software-support-policy/">https://www.isc.org/downloads/software-support-policy/</link>
      for details of ISC's software support policy.
    </para>
  </section>

  <section xml:id="relnotes_thanks"><info><title>Thank You</title></info>
    <para>
      Thank you to everyone who assisted us in making this release possible.
      If you would like to contribute to ISC to assist us in continuing to
      make quality open source software, please visit our donations page at
      <link xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="http://www.isc.org/donate/">http://www.isc.org/donate/</link>.
    </para>
  </section>
</section>
