Source: bind
Section: net
Priority: optional
Maintainer: BIND <bind@packages.debian.org>
Uploaders: Ondřej Surý <ondrej@debian.org>,
           Bernhard Schmidt <berni@debian.org>
Build-Depends: bison,
               debhelper (>= 10),
               dh-apparmor,
               dh-exec,
               dh-python,
               dpkg-dev (>= 1.16.1~),
               libcap2-dev [!kfreebsd-i386 !kfreebsd-amd64 !hurd-i386],
               libdb-dev (>>4.6),
               libgeoip-dev (>= 1.4.6.dfsg-5),
               libjson-c-dev,
               libkrb5-dev,
               libldap2-dev,
               liblmdb-dev,
               libssl-dev,
               libtool,
               libxml2-dev,
               python3,
               python3-ply
Standards-Version: 4.1.2
Vcs-Browser: https://salsa.debian.org/dns-team/bind
Vcs-Git: https://salsa.debian.org/dns-team/bind.git
Homepage: https://www.isc.org/downloads/bind/

Package: bind
Architecture: any
Depends: adduser,
         bind-libs (= ${binary:Version}),
         bind-utils (= ${binary:Version}),
         debconf | debconf-2.0,
         lsb-base (>= 3.2-14),
         net-tools,
         netbase,
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: bind-doc,
          dnsutils,
          resolvconf,
          ufw
Breaks: bind9 (<< 1:9.12.0~)
Replaces: bind9 (<< 1:9.12.0~)
Description: Internet Domain Name Server
 The Berkeley Internet Name Domain (BIND) implements an Internet domain
 name server.  BIND is the most widely-used name server software on the
 Internet, and is supported by the Internet Software Consortium, www.isc.org.
 .
 This package provides the server and related configuration files.

Package: bind-utils
Architecture: any
Depends: python3,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Breaks: bind9utils (<< 1:9.12.0~)
Replaces: bind9utils (<< 1:9.12.0~)
Description: Utilities for BIND
 This package provides various utilities that are useful for maintaining a
 working BIND installation.

Package: bind-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Breaks: bind9-doc (<< 1:9.12.0~)
Replaces: bind9-doc (<< 1:9.12.0~)
Description: Documentation for BIND
 This package provides various documents that are useful for maintaining a
 working BIND installation.

Package: bind-host
Priority: standard
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Provides: host
Breaks: bind9-host (<< 1:9.12.0~)
Replaces: bind9-host (<< 1:9.12.0~)
Description: DNS Lookup Utility
 This package provides the 'host' DNS lookup utility in the form that
 is bundled with the BIND sources.

Package: bind-dev
Section: libdevel
Architecture: any
Breaks: libbind-dev (<< 1:9.12.0~)
Replaces: libbind-dev (<< 1:9.12.0~)
Depends: bind-libs (= ${binary:Version}),
         ${misc:Depends}
Description: Static Libraries and Headers used by BIND
 This package delivers archive-style libraries, header files, and API man
 pages for libbind, libdns, libisc, and liblwres.  These are only needed
 if you want to compile other packages that need more nameserver API than the
 resolver code provided in libc.

Package: bind-libs
Section: libs
Priority: standard
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Shared Libraries used by BIND
 The Berkeley Internet Name Domain (BIND) implements an Internet domain
 name server.  BIND is the most widely-used name server software on the
 Internet, and is supported by the Internet Software Consortium, www.isc.org.
 .
 This package contains a bundle of shared libraries used by BIND.

Package: bind-dnsutils
Priority: standard
Architecture: any
Depends: bind-host | host,
         bind-libs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: dnsutils (<< 1:9.12.0~)
Conflicts: dnsutils (<< 1:9.12.0~)
Provides: dnsutils
Description: Clients provided with BIND
 The Berkeley Internet Name Domain (BIND) implements an Internet domain
 name server.  BIND is the most widely-used name server software on the
 Internet, and is supported by the Internet Software Consortium, www.isc.org.
 .
 This package delivers various client programs related to DNS that are
 derived from the BIND source tree.
 .
  - dig - query the DNS in various ways
  - nslookup - the older way to do it
  - nsupdate - perform dynamic updates (See RFC2136)
